# 仓库镜像管理功能开通申请

### 🎭功能介绍

仓库镜像管理功能用于配置和管理仓库镜像；配置仓库镜像可以实现不同平台之间仓库分支、标签和提交信息的自动同步。

### 📄申请方式

- 开通范围：仓库是 GVP 或推荐项目，满足其一即可。

- 申请方式：在评论区留下 **仓库地址** 及 **项目作者**。 

例：

仓库地址：https://gitee.com/gitee-community/opensource-guide

项目作者：Gitee

-  **开通通知：**  **报名成功后，将于7日内通过私信告知已开通，如在7日内未收到任何信息，可能您的仓库不在开通范围或仓库地址有误。如地址有误请私信工作人员修改** 

### 💡功能使用文档

Gitee 支持设置两种镜像：

Push：用于将 Gitee 的仓库自动镜像到 GitHub 。

配置此镜像后，当你提交代码到 Gitee 的仓库时，Gitee 会自动向 GitHub 同步仓库。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0625/151440_6c742fa0_8189591.png "屏幕截图.png")



Pull：用于将 GitHub 的仓库镜像到 Gitee 。

你可以根据自身需求选择 自动镜像 或 手动镜像；

自动镜像：当你提交代码到 GitHub 镜像仓库时，Gitee 会自动从 GitHub 同步仓库；

![输入图片说明](https://images.gitee.com/uploads/images/2021/0625/151458_778c4caa_8189591.png "屏幕截图.png")


手动镜像：只有你手动点击更新按键时，Gitee 才会从 GitHub 同步仓库。


 **想要 Get 更多使用技巧，点击查看详情：https://gitee.com/help/articles/4336** 